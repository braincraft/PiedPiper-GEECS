class ReportsController < ApplicationController
  load_and_authorize_resource
  before_action :set_report, only: [:show, :destroy, :resolve]

  # GET /reports
  def index
    if current_user.official?
      if params[:search]
        @reports = Report.page(params[:page]).search(params).order(created_at: :desc)
      else
        @reports = Report.page(params[:page]).where('resolved = ?', false).order(created_at: :desc)
      end
    else
      @reports = current_user.reports.page(params[:page]).order(created_at: :desc)
    end
  end

  # GET /reports/summary
  def summary
    @reports = Report.all
    @chart = GoogleVisualr::Interactive::BarChart.new(City::make_chart(@reports), City::chart_options)
  end

  # GET /reports/1
  def show
  end

  # GET /reports/new
  def new
    @report = Report.new
  end

  # POST /reports
  def create
    @report = Report.new(report_params)
    if @report.save
      if params[:image_files]
        params[:image_files].each do |image|
          @report.image_files.create(content: image)
        end
      end
      redirect_to root_path, notice: 'Report was successfully submitted.'
    else
      render :new
    end
  end

  # DELETE /reports/1
  def destroy
    @report.destroy
    redirect_to reports_path, notice: 'Report was successfully rejected.'
  end
  
  # PATCH /reports/resolve
  def resolve
    if @report.update(resolved: true)
      redirect_to @report, notice: 'Report was successfully resolved.'
    else
      redirect_to @report, alert: 'Report could not be resolved.'
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_report
      @report = Report.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def report_params
      params.require(:report).permit(:user_id, :city_id, :plate_number, :description)
    end
end
