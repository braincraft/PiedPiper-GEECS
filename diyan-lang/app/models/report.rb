# == Schema Information
#
# Table name: reports
#
#  id           :integer          not null, primary key
#  user_id      :integer
#  city_id      :integer
#  plate_number :string
#  description  :text
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  resolved     :boolean          default(FALSE)
#  first_name   :string
#  last_name    :string
#

class Report < ActiveRecord::Base
  belongs_to :user

  has_many :image_files, :dependent => :destroy

  self.per_page = 5

  def self.random_plate_number
    "#{('a'..'z').to_a.shuffle[0..2].join.upcase}-#{('0'..'9').to_a.shuffle[0..2].join}"
  end

  scope :search, ->(params) { 
    s = params[:search]
    result = all
    result = result.where('city_id = ?', s[:city]) unless s[:city].blank?
    result = result.where('plate_number LIKE ?', "%#{s[:plate_number]}%") unless s[:plate_number].blank?
    # result = result.where('EXTRACT(year FROMcreated_at) >= ?', s["start_date(1i)"].to_i) unless s["start_date(1i)"].blank?
    # result = result.where('end_date <= ?', s[:end_date]) unless s[:end_date].blank?
    result = result.where('resolved = ?', false) unless s[:show_resolved]
  }
end
