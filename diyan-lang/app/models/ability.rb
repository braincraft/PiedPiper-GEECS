class Ability
  include CanCan::Ability

  def initialize(user)
    user ||= User.new

    if user.official?
      can :manage, Report
    elsif user.id
      can :read, Report, user_id: user.id
    end

    can :create, User
    can :create, Report    
  end
end