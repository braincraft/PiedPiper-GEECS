# == Schema Information
#
# Table name: image_files
#
#  id                   :integer          not null, primary key
#  report_id            :integer
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  content_file_name    :string
#  content_content_type :string
#  content_file_size    :integer
#  content_updated_at   :datetime
#

class ImageFile < ActiveRecord::Base
  belongs_to :report

  has_attached_file :content, styles: { medium: "300x300>", thumb: "32x32>" }, default_url: "/images/:style/missing.png"

  validates_attachment_content_type :content, :content_type => /\Aimage\/.*\Z/
end
