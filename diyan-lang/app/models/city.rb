class City
  NCR = [
    "Quezon City", "Pateros", "Caloocan", "Las Pinas", "Makati",
    "Malabon", "Mandaluyong", "Marikina", "Muntinlupa", "Navotas",
    "Paranaque", "Pasay", "Pasig", "San Juan", "Taguig",
    "Valenzuela"
  ]

  def self.city_code(city)
    NCR.index city
  end

  def self.make_chart(reports = Report.all)
    data_table = GoogleVisualr::DataTable.new

    # Add Column Headers
    data_table.new_column('string', 'City' )
    data_table.new_column('number', 'No')
    data_table.new_column('number', 'Yes')

    # Add Rows and Values
    rows = []
    NCR.count.times do |i|
      if reports.where('city_id = ?', i).count > 0
        rows.push [NCR[i], reports.where('city_id = ? AND resolved = ?', i, false).count, reports.where('city_id = ? AND resolved = ?', i, true).count]
      end        
    end
    data_table.add_rows rows

    data_table
  end

  def self.chart_options
    { title: 'Resolved and Unresolved Reports',
      width: 600, 
      height: 600, 
      fontName: 'Lato',
      colors: ['#191932', '#393b81'],
      hAxis: { ticks: [ 0, 1, 2, 3, 4 ] } }
  end

end