module ApplicationHelper
  def sortable(column, title = nil)
    title ||= column.titleize
    css_class = column == sort_column ? "current #{sort_direction}" : nil
    direction = column == sort_column && sort_direction == "asc" ? "desc" : "asc"
    link_to title, params.merge(sort: column, direction: direction, page: nil), { :class => css_class }
  end

  def link_to_function(name, *args, &block)
    html_options = args.extract_options!.symbolize_keys

    function = block_given? ? update_page(&block) : args[0] || ''
    onclick = "#{"#{html_options[:onclick]}; " if html_options[:onclick]}#{function}; return false;"
    href = html_options[:href] || '#'

    content_tag(:a, name, html_options.merge(:href => href, :onclick => onclick))
  end

  def link_to_add_fields(name, f, association, selector, partial="")
    new_object = f.object.class.reflect_on_association(association).klass.new
    fields = f.fields_for(association, new_object, :child_index => "new_#{association}") do |builder|
      if (partial == "")
        render(association.to_s.singularize + "_fields", f: builder)
      else
        render(partial, f: builder)
      end

    end
    js_args = '"' + "#{selector}" + '", '
    js_args += '"' + "#{association}" + '", '
    js_args += '"' + "#{escape_javascript(fields)}" + '"'
    link_to_function(name, "add_fields(#{js_args})")
  end

  def link_to_remove_fields(item, builder, text="")
    render "destroy_link", f: builder, text: text
  end

  def destroy_check_box(builder, text="")
    render "destroy_check_box", f: builder, text: text
  end

  # change the default link renderer for will_paginate
  def will_paginate(collection_or_options = nil, options = {})
    if collection_or_options.is_a? Hash
      options, collection_or_options = collection_or_options, nil
    end
    unless options[:renderer]
      options = options.merge renderer: SemanticLinkRenderer
    end
    super *[collection_or_options, options].compact
  end

  # renderer
  class SemanticLinkRenderer < WillPaginate::ActionView::LinkRenderer
    def page_number(page)
      unless page == current_page
        link(page, page, :rel => rel_value(page), :class => 'item')
      else
        tag(:div, page, :class => 'current item')
      end
    end

    def previous_or_next_page(page, text, classname)
      if page
        link(text, page, :class => classname + ' item')
      else
        tag(:div, text, :class => classname + ' item disabled')
      end
    end
  end
end
