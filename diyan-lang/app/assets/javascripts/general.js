function remove_fields (link) {
  $(link).prev("input[type=hidden]").val(1);
  $(link).closest(".dynamic.field").hide();
}

function add_fields (selector, association, content) {
  var new_id = new Date().getTime();
  var regexp = new RegExp("new_" + association, "g");
  $(selector).append(content.replace(regexp, new_id));
}