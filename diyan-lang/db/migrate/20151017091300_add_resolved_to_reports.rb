class AddResolvedToReports < ActiveRecord::Migration
  def change
    add_column :reports, :resolved, :boolean
  end
end
