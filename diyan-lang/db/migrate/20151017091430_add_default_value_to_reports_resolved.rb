class AddDefaultValueToReportsResolved < ActiveRecord::Migration
  def change
    remove_column :reports, :resolved
    add_column :reports, :resolved, :integer, default: false
  end
end
