class AddDefaultValueToUsersOfficial < ActiveRecord::Migration
  def change
    remove_column :users, :official
    add_column :users, :official, :boolean, default: false
  end
end
