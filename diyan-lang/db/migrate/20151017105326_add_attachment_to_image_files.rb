class AddAttachmentToImageFiles < ActiveRecord::Migration
  def change
    add_attachment :image_files, :content
  end
end
