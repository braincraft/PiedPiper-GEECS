class FixReportResolvedDataType < ActiveRecord::Migration
  def change
    remove_column :reports, :resolved
    add_column :reports, :resolved, :boolean, default: false
  end
end
