class CreateReports < ActiveRecord::Migration
  def change
    create_table :reports do |t|
      t.integer :user_id
      t.integer :city_id
      t.string :plate_number
      t.text :description

      t.timestamps null: false
    end
  end
end
