class AddNameToReport < ActiveRecord::Migration
  def change
    add_column :reports, :first_name, :string
    add_column :reports, :last_name, :string
  end
end
