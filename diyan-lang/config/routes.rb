Rails.application.routes.draw do
  root 'static#index'

  devise_for :users

  resources :reports, except: [:show, :edit, :update] do
    get 'summary', on: :collection, as: :summary
    patch 'resolve', on: :member, as: :resolve
  end
end