# == Schema Information
#
# Table name: reports
#
#  id           :integer          not null, primary key
#  user_id      :integer
#  city_id      :integer
#  plate_number :string
#  description  :text
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  resolved     :boolean          default(FALSE)
#  first_name   :string
#  last_name    :string
#

FactoryGirl.define do
  factory :report do
    user { User.find(rand(User.count).to_i + 1) || create(:user) }
    city_id { rand City::NCR.count }
    plate_number { Report.random_plate_number }
    sequence(:description) { Faker::Lorem.paragraph }
  end

end
