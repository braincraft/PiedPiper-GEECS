# == Schema Information
#
# Table name: reports
#
#  id           :integer          not null, primary key
#  user_id      :integer
#  city_id      :integer
#  plate_number :string
#  description  :text
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  resolved     :boolean          default(FALSE)
#

FactoryGirl.define do
  factory :user do
    first_name { Faker::Name.first_name }
    last_name { Faker::Name.last_name }
    username { "#{first_name.downcase unless first_name.nil?}.#{last_name.downcase unless last_name.nil?}" }
    email { "#{username}@example.com" }
    official false
    password "password"
    encrypted_password { User.new.send(:password_digest, "#{password}") }

    factory :official do
      first_name "Government"
      last_name "Mule"
      username "government.mule"
      email "government.mule@mmda.gov.ph"
      official true
    end
  end

end
