# == Schema Information
#
# Table name: image_files
#
#  id                   :integer          not null, primary key
#  report_id            :integer
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  content_file_name    :string
#  content_content_type :string
#  content_file_size    :integer
#  content_updated_at   :datetime
#

FactoryGirl.define do
  factory :image_file do
    report_id 1
  end

end
