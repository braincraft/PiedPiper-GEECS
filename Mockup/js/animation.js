var back_child=[];
var front_child=[];





$(function(){

  $("#li1").css("animation","beatGreen 0.5s ease infinite");


  //5 vehicles on Front road
  genImage("jeep","front");
  genImage("pedicab","front");
  genImage("jeep","front");
  genImage("taxi","front");
  genImage("bus2","front");
  var frontVehicles = $("#front-road img");
  genImage("jeep","back");
  genImage("pedicab","back");
  genImage("jeep","back");
  genImage("bus1","back");
  genImage("bus2","back");
  genImage("taxi","back");
  genImage("bus2","back");
  var backVehicles = $("#back-road img");

  //Front
  TweenMax.to(frontVehicles[0],5,{marginLeft:window.innerWidth,ease:Power0.easeInOut, repeat:-1})
  TweenMax.to(frontVehicles[1],5.5,{marginLeft:window.innerWidth,ease:Power0.easeInOut, repeat:-1,delay:1});
  TweenMax.to(frontVehicles[2],4.5,{marginLeft:window.innerWidth,ease:Power1.easeOut, repeat:-1,delay:1.5});
  TweenMax.to(frontVehicles[3],4,{marginLeft:window.innerWidth,ease:Power2.easeOut, repeat:-1,delay:0.5});
  TweenMax.to(frontVehicles[4],5,{marginLeft:window.innerWidth,ease:Power0.easeInOut, repeat:-1,delay:0.25});


  //Back
  TweenMax.to(backVehicles[0],5,{marginRight:window.innerWidth,ease:Power0.easeInOut, repeat:-1})
  TweenMax.to(backVehicles[1],5.5,{marginRight:window.innerWidth,ease:Power0.easeInOut, repeat:-1,delay:1});
  TweenMax.to(backVehicles[2],4.5,{marginRight:window.innerWidth,ease:Power1.easeOut, repeat:-1,delay:1.5});
  TweenMax.to(backVehicles[3],4,{marginRight:window.innerWidth,ease:Power2.easeOut, repeat:-1,delay:0.5});
  TweenMax.to(backVehicles[4],5,{marginRight:window.innerWidth,ease:Power0.easeInOut, repeat:-1,delay:0.25});
  TweenMax.to(backVehicles[5],4.5,{marginRight:window.innerWidth,ease:Power1.easeOut, repeat:-1,delay:1.5});
  TweenMax.to(backVehicles[6],4,{marginRight:window.innerWidth,ease:Power2.easeOut, repeat:-1,delay:0.5});
  //TweenMax.to(backVehicles[7],5,{marginRight:window.innerWidth,ease:Power0.easeInOut, repeat:-1,delay:0.25});

  $("#li2").click(function(){
      TweenMax.globalTimeScale(0.15);
  });
  $("#li3").click(function(){
    TweenMax.pauseAll();
  });

  $("#li1").click(function(){
    TweenMax.resumeAll();
    TweenMax.globalTimeScale(1);
  });


});


function genImage(vehicle,location){
  var elem=document.createElement("img");
  elem.setAttribute("class","img-responsive");
  if(location=="back")
    {
    elem.src='images/left/'+vehicle+'.png';
    elem.setAttribute("class","img-responsive");
    elem.style.animation="slideToLeft 5s ease infinite";
    }
  else {
    elem.src='images/right/'+vehicle+'.png';
    elem.setAttribute("class","img-responsive");
  }
  if(location=="back")
    {
      $('#back-road').append(elem);
      back_child.push(elem);
    }
  else {
      $('#front-road').append(elem);
      front_child.push(elem);
  }
}
