package com.incodeables.braincraft.diyanlang;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

public class Home extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        findViewById(R.id.create_notif).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplication(),MainActivity.class );
                PendingIntent pIntent = PendingIntent.getActivity(getApplicationContext(),0,intent,0);
                Notification noti=new Notification.Builder(getApplicationContext())
                        .setTicker("Report: Traffic Violation")
                        .setContentTitle("Report from Good Person 1")
                        .setContentText("Traffic Violation happened today.")
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentIntent(pIntent).getNotification();
                noti.flags=Notification.FLAG_AUTO_CANCEL;
                ((NotificationManager)getSystemService(NOTIFICATION_SERVICE)).notify(0,noti);
            }
        });
    }

    @Override
    protected void onPause(){
        super.onPause();
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                //Toast.makeText(Home.this, "Test", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(getApplication(),MainActivity.class );
                PendingIntent pIntent = PendingIntent.getActivity(getApplicationContext(),0,intent,0);
                Notification noti=new Notification.Builder(getApplicationContext())
                        .setTicker("Report: Traffic Violation")
                        .setContentTitle("Report from Good Person 1")
                        .setContentText("Traffic Violation happened today.")
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentIntent(pIntent).getNotification();
                noti.flags=Notification.FLAG_AUTO_CANCEL;
                ((NotificationManager)getSystemService(NOTIFICATION_SERVICE)).notify(0,noti);
            }
        }, 3000);

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
